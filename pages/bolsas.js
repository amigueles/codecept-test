
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
	   I   = require('../steps_file.js')();
  },

  validateBolsas(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkMisBolsasMenu'});
  	I.waitInUrl('/bolsas', 6);
  	I.see('Mis Bolsas_');
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_MisBolsas' + '_' + '.png', true);

    I.click({id: 'linkContratarBolsas'});
    I.see('Comprar bolsa_');
    I.waitInUrl('/bolsas/disponibles', 6);
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsas' + '_' + '.png', true);

    I.wait(3);
    I.click({id: 'linkBolsasNavegacion'});
    I.see('Comprar bolsa_');
    I.waitInUrl('/bolsas/datospp', 6);
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasNavegacion' + '_' + '.png', true);
    I.click({id: 'linkContratar'});
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasNavegacionP2' + '_' + '.png', true);
    I.click({xpath: '//div[@class="collap-default-head"]//a'});
    I.click('div[class="on-off-switch-text on-off-switch-text-off"]');
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasNavegacionP2OK' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.click({id: 'boton-atras'});

    I.wait(3);
    I.click({id: 'linkBolsasVoz'});
    I.see('Comprar bolsa_');
    I.waitInUrl('/bolsas/vozpp', 6);
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasVoz' + '_' + '.png', true);
    I.click({id: 'linkContratar'});
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasVozP2' + '_' + '.png', true);
    I.click({xpath: '//div[@class="collap-default-head"]//a'});
    I.click('div[class="on-off-switch-text on-off-switch-text-off"]');
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasVozP2OK' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.click({id: 'boton-atras'});

    I.wait(3);
    I.click({id: 'linkBolsasSMS'});
    I.see('Comprar bolsa_');
    I.waitInUrl('/bolsas/mensajespp', 6);
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasMensajes' + '_' + '.png', true);
    I.click({id: 'linkContratar'});
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasMensajesP2' + '_' + '.png', true);
    I.click({xpath: '//div[@class="collap-default-head"]//a'});
    I.click('div[class="on-off-switch-text on-off-switch-text-off"]');
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasMensajesP2OK' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.click({id: 'boton-atras'});

    I.wait(3);
    I.click({id: 'linkBolsasCombinadas'});
    I.see('Comprar bolsa_');
    I.waitInUrl('/bolsas/combinadaspp', 6);
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasCombinadas' + '_' + '.png', true);
    I.click({id: 'linkContratar'});
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasCombinadasP2' + '_' + '.png', true);
    I.click({xpath: '//div[@class="collap-default-head"]//a'});
    I.click('div[class="on-off-switch-text on-off-switch-text-off"]');
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasCombinadasP2OK' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.click({id: 'boton-atras'})


    I.click({id: 'img_banner'});
    I.see('Comprar bolsa_');
    I.waitInUrl('/dbpVoz?idBolsa=vozPPS&carta=PO_ADDON_BOLSA_VOZ_ILIM7D', 6);
    I.wait(3);
    I.saveScreenshot(contador + '_Bolsas_' + numero + '_ComprarBolsasBanner' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
  }  
}
