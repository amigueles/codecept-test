
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validateProductos(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkContratarNuevosProductosMenu'});
    I.waitInUrl('/contratarNuevos', 6);
    I.see('Contratar nuevos productos_');
    I.wait(3);
    I.saveScreenshot(contador + '_nuevosProductos_' + numero + '_Home' + '_' + '.png', true); 
  }
}
