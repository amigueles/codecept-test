'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
    I          = require('../steps_file.js')();
    dateformat = require('dateformat');
  },

  validateDashboard(menuPpal, timeToWait) {
    I.waitInUrl('/' + menuPpal, timeToWait);
  	I.wait(15);
    
    //si es el dashboard de rediseñoPP...
    if (menuPpal === 'dashboard') {
      I.waitForText('Hola', 10);
      I.see('Presta Luka');
      I.see('Saldo');
      I.see('Bolsas');
    //si es el dashboard legacy...
    } else {
      I.waitForText('Bienvenido', 10);
    }
  },

  accederPrestaLuka(numero) {
    this.acceder(numero, "prestaLuka");      
  },

  accederRecargar(numero) {
    this.acceder(numero, "recargar");        
  },

  accederRecargarBolsas(numero) {
    this.irDashboard();
    I.click('div[data-key=bolsas]');
    I.click('a[class="btn-recargar-pre col-12 pull-right detalle clearfix"]');      
    this.screenshot(numero, "_Dashboard","_RecargarBolsas"); 
  },

  accederComprarBolsas(numero) {
    this.irDashboard();
    I.click('div[data-key=bolsas]');
    I.click('a[class="btn-orange-pre col-12 pull-right detalle clearfix mt-15 shadow-base"]');
    this.screenshot(numero, "_Dashboard","_ComprarBolsas");      
  },

  accederSaldoReservado(saldoReservado, numero) {
    this.tipoSaldo(numero, saldoReservado, "reservado");
  },

  accederBonoExtra(bonoExtra, numero) {
    this.tipoSaldo(numero, bonoExtra, "BonoExtra");
  },

  accederBolsas(numero) {
    I.wait(3);
    I.click('div[data-key=bolsas]');
    this.screenshot(numero, "_Dashboard","_Bolsas");
  },

  accederTipoBolsas(botonSMS, botonVoz, botonDatos, numero) {
    this.tipoBolsa(numero, botonSMS, "SMS");
    this.tipoBolsa(numero, botonVoz, "Voz");
    this.tipoBolsa(numero, botonDatos, "Datos");
   },

  verDetalleBolsas(detalleBolsas, numero){
    if (detalleBolsas > 0) {
      this.verDetalle(numero, "_VerDetalleBolsas")
    }
  },

  verDetalleRRSS(verDetalleRRSS, numero) {
    if ( verDetalleRRSS > 0 ) {
      this.verDetalle(numero, "_VerDetalleRRSS");
    }
  },

  accederOfertas(numero) {
    this.linkOferta(numero, "", "" );
    this.linkOferta(numero, "LinkBannerPromoBolsaVozPP", "_OfertasPromoBolsaVozPP" );
    this.linkOferta(numero, "linkBannerCinemark", "_OfertasBannerCinemark" );
    this.linkOferta(numero, "bannerSumate", "_OfertasbannerSumate" );
  },

  welcome(titulo){
    var tituloLower = titulo.toLowerCase();
    if (tituloLower.includes('welcome')) {
      I.say('Cliente ya no es prepago');
      I.click({id: 'imgWelcomeCambioPlan'});
      //dashboardPage.validateDashboard('dashboard', 5);
    }
  },

  //methods
  screenshot(numero, page, etapa){
    I.wait(3);
    I.saveScreenshot(numero + page + etapa + '.png', true);
  },

  irDashboard(){
    //Agregar validacion de solo ejecutar si no es inicio
    I.click('a[id=botonInicio]');
    I.wait(5);
  },

  acceder(numero, tipo){
    this.irDashboard();
    I.click('div[function='+tipo+']');
    this.screenshot(numero, "_Dashboard_", tipo);  
  },
  
  verDetalle(numero, etapa){
    I.click('a[data-modal=solicitar]');
    this.screenshot(numero, "_Dashboard", etapa);
    I.click({xpath: '//div[@class="modal-screen solicitar"]//div[@class="modal-content-prepago"]//a[@class="close-x"]'});
  },

  tipoBolsa(numero, botonVisible, tipo){
    if (botonVisible > 0) {
      I.click('div[function='+tipo+']');
      this.screenshot(numero, "_Dashboard_", tipo);
      this.verDetalle(numero, "_VerDetalleBolsas", tipo);
    }
   },

  tipoSaldo(numero, visible, tipo){
    if ( visible > 0 ) {
      I.click('div[function='+tipo+']');
      this.screenshot(numero, "_Dashboard_", tipo); 
      I.click('a[data-modal=bono]');
      this.screenshot(numero, "_Dashboard_info", tipo); 
      I.click('a[class="close-x-exclamacion"]');
    }
  },

  linkOferta(numero, link, etapa ){
    this.irDashboard();
    I.click('a[id=boton-ofertas]'); 
    if (link === ""){
      this.screenshot(numero, "_Dashboard","_Ofertas");
      I.click({xpath: '//div[@class="modal-screen oferta"]//div[@class="modal-content-prepago"]//a[@class="close-x"]'});
    }else{
      I.click('a[id='+link+']');
      this.screenshot(numero, "_Dashboard",etapa);
    }   
  }
}