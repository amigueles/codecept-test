
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validateTarifa(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkMiTarifaMenu'});
    I.waitInUrl('/mi_tarifa', 6);
    I.see('Mi tarifa_');
    I.wait(3);
    I.saveScreenshot(contador + '_MiTarifa_' + numero + '_Home' + '_' + '.png', true);
  }
}
