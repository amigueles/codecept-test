
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validateTiendas(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkTiendasFlecha'});
    I.waitInUrl('/tiendas', 6);
    I.see('Tiendas_');
    I.wait(3);
    I.saveScreenshot(contador + '_Tiendas_' + numero + '_Home' + '_' + '.png', true);
  }
}
