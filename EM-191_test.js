Feature('Beneficios por recarga');

Before((I) => { // or Background
  I.amOnPage('http://localhost:7001/mientel/login.action');
});

After((I, menuPrincipalPage) => {
  	menuPrincipalPage.logout();
});

Scenario('Recarga...', (I, loginPage, dashboardPage, recargaPage) => {
	loginPage.validateForm();
	loginPage.sendForm('942002588', '205585141', '6490');

	dashboardPage.validateDashboard('dashboard');

	I.seeElement({id: 'recargar'});
	I.click('#recargar');
	recargaPage.validateRecarga();
	pause();
});