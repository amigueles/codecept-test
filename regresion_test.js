Feature('Test App');

let accounts         = new DataTable(['telefono']);
let readEachLineSync = require('read-each-line-sync')
let contador         = 0;
let log        		 = require('log-to-file');

Before(function* (I, loginPage, dashboardPage) {
	I.amOnPage('http://localhost:7001/mientel/login.action');
	loginPage.validateForm();

	/*En el caso que se necesite leer datos desde un archivo
	se debe cambiar el parametro numero de loginPage.sendForm  por
	current.telefono y ademas mover al Scenario a utilizar
	desde loginPage.sendForm hasta dashboardPage.welcome
	
	loginPage.sendForm('994832903', '13041121', '1991');

	let portado = yield I.grabNumberOfVisibleElements('div[class=jContent]');
	if (portado > 0) {
		let lightBox = yield I.grabTextFrom('div[class=jContent]');
		if (lightBox.includes('El Número ingresado no corresponde a Entel')) {
			I.say('Ya no es cliente Entel');
			I.click('OK');
		}
	}
	I.waitForElement('div[data-key=bolsas]',20);

	let titulo = yield I.grabTitle();
	dashboardPage.welcome(titulo); 
	I.wait(5);*/
});

After(function* (I, menuPrincipalPage) {
	I.wait(5);
	let botonVerMas = yield I.grabNumberOfVisibleElements('a[id=botonVerMas]');
	if (botonVerMas > 0) {
  		menuPrincipalPage.logout();
	}
});

readEachLineSync('telefonos.txt', function(line) {
  accounts.add([line]);
});

/*
Data(accounts).only.Scenario('Session...', function* (I, loginPage, utilsPage, current) {
	utilsPage.validateSession(++contador, current.telefono);
});
*/

Data(accounts).only.Scenario('Regresion...', function* (I, loginPage, dashboardPage, recargaPage, zonaPage, productosPage, ayudaSoportePage, prestaLukaPage, puntosDeReciclajePage, bolsasPage, tiendasPage, tarifaPage, current) {

	//let tarifa = yield I.grabTextFrom('#tarifa');
	//log('Telefono|' + current.telefono + '|Tarifa|' + tarifa);

	loginPage.sendForm('942002588', '13041121', '1991');

	let portado = yield I.grabNumberOfVisibleElements('div[class=jContent]');
	if (portado > 0) {
		let lightBox = yield I.grabTextFrom('div[class=jContent]');
		if (lightBox.includes('El Número ingresado no corresponde a Entel')) {
			I.say('Ya no es cliente Entel');
			I.click('OK');
		}
	}
	I.waitForElement('div[data-key=bolsas]',20);

	let titulo = yield I.grabTitle();
	dashboardPage.welcome(titulo);

	I.wait(5);
	dashboardPage.screenshot(current.telefono, "_Dashboard","");  

	let saldoReservado = yield I.grabNumberOfVisibleElements('div[function=reservado');
 	dashboardPage.accederSaldoReservado(saldoReservado, current.telefono);

	let bonoExtra = yield I.grabNumberOfVisibleElements('div[function=bonoExtra]');
	dashboardPage.accederBonoExtra(bonoExtra, current.telefono);

	let verDetalleRRSS = yield I.grabNumberOfVisibleElements('a[data-modal=solicitar]');
	dashboardPage.verDetalleRRSS(verDetalleRRSS, current.telefono);
	
	//dashboardPage.accederPrestaLuka(current.telefono);
	//dashboardPage.accederOfertas(current.telefono);
	//dashboardPage.accederRecargar(current.telefono);
	
	dashboardPage.accederBolsas(current.telefono, ++contador);

	let botonSMS = yield I.grabNumberOfVisibleElements('div[function=SMS]');
	let botonVoz = yield I.grabNumberOfVisibleElements('div[function=Voz]');
	let botonDatos = yield I.grabNumberOfVisibleElements('div[function=Datos]');
	dashboardPage.accederTipoBolsas(botonSMS, botonVoz, botonDatos, current.telefono);

  //dashboardPage.accederRecargarBolsas(current.telefono, ++contador);
	//dashboardPage.accederComprarBolsas(current.telefono, ++contador);

	dashboardPage.accederOfertas(++contador, current.telefono);

	bolsasPage.validateBolsas(++contador, current.telefono);

	productosPage.validateProductos(++contador, current.telefono);

	tarifaPage.validateTarifa(++contador, current.telefono);

	prestaLukaPage.validatePrestaLuka(++contador, current.telefono);

	tiendasPage.validateTiendas(++contador, current.telefono);

	zonaPage.validateZona(++contador, current.telefono);

	ayudaSoportePage.validateAyudaSoporte(++contador, current.telefono);

	puntosDeReciclajePage.validatePuntosDeReciclaje(++contador, current.telefono);

	recargaPage.validateRecarga(++contador, current.telefono);

});