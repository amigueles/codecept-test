'use strict';

let I;

module.exports = {

  _init() {
    I = require('../steps_file.js')();
  },

  // setting locators
  fields: {
	msisdn : '#msisdn',
	rut    : '#rut',
	pin    : '#pin'
  },
  submitButton: '#entrar',

  // introducing methods
  validateForm() {
  	I.waitInUrl('/login', 6);
  	I.see('[ Telefonía Móvil_');
  	I.seeElement({id: 'msisdn'});
  	I.seeElement({id: 'rut'});
  	I.seeElement({id: 'pin'});
  	I.seeElement({id: 'entrar'});
  },

  sendForm(msisdn, rut, pin) {
    I.fillField(this.fields.msisdn, msisdn);
    I.fillField(this.fields.rut, rut);
    I.fillField(this.fields.pin, pin);

    I.click(this.submitButton);
  }
}
