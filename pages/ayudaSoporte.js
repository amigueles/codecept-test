
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validateAyudaSoporte(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkAyudaSoporteFlecha'});
    I.waitInUrl('/ayudaYSoporte', 6);
    I.see('Ayuda y Soporte_');
    I.wait(3);
    I.saveScreenshot(contador + '_AyudaSoporte_' + numero + '_Home' + '_' + '.png', true);
  }
}
