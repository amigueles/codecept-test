'use strict';

let I;

module.exports = {

  _init() {
    I = require('../steps_file.js')();
  },

  // insert your locators and methods here
  logout() {
    I.click({id: 'botonVerMas'});
  	I.wait(3);
  	I.scrollPageToBottom();
  	I.click({id: 'linkLogout'});
  	I.wait(5);
  	/*I.say('Fin del Test');
  	I.say('Presione Ctrl+C y luego S+Enter para salir');*/
  }
}