
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validatePuntosDeReciclaje(contador, numero) {
     I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'puntosDeReciclajeFlecha'});
    I.waitInUrl('/puntosDeReciclaje', 6);
    I.see('Puntos de Reciclaj');
    I.wait(3);
    I.saveScreenshot(contador + '_PuntosDeReciclaje_' + numero + '_Home' + '_' + '.png', true);
  }
}
