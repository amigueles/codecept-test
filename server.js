var selenium = require('selenium-standalone');

module.exports = {
  bootstrap: function(done) {
    selenium.start(done);
  }
}
