
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validateZona(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkBeneficiosZonaFlecha'});
    I.waitInUrl('/zona.action', 6);
    I.see('Beneficios zOna_');
    I.wait(3);
    I.saveScreenshot(contador + '_BeneficiosZona_' + numero + '_Home' + '_' + '.png', true);
    I.scrollPageToBottom();
     I.saveScreenshot(contador + '_BeneficiosZona_' + numero + '_Home2' + '_' + '.png', true);
  }
}
