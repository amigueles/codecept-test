'use strict';
let beautify;

Feature('Rediseño PP');

Before((I) => { // or Background
  I.amOnPage('http://appswlspre.entel.cl/mientel/evo/login.action');
});

After((I, menuPrincipalPage) => {
  	menuPrincipalPage.logout();
});

Scenario('Dashboard...', function* (I, loginPage, dashboardPage) {
	
	loginPage.validateForm();
	loginPage.sendForm('996161647', '13041121', '1991');

	dashboardPage.validateDashboard('menu_ppal');
	
	dashboardPage.screenshot('01','996161647');

	let dData = yield I.executeScript(function() {
      return digitalData;
    });

	beautify = require("json-beautify");

	I.say('digitalData: ' + beautify(dData, null, 4, 80));
});

Scenario('Recarga...', (I, loginPage, dashboardPage, recargaPage) => {
	loginPage.validateForm();
	loginPage.sendForm('996161647', '13041121', '1991');

	dashboardPage.validateDashboard('menu_ppal');

	I.seeElement({id: 'recargar'});
	I.click('#recargar');
	recargaPage.validateRecarga();
	recargaPage.screenshot();
});