
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
     I   = require('../steps_file.js')();
  },

  validatePrestaLuka(contador, numero) {
    I.click({id: 'botonVerMas'});
    I.wait(3);
    I.click({id: 'linkPrestaLukaMenu'});
    I.waitInUrl('/prestaluka', 6);
    I.see('Presta Luka_');
    I.wait(3);
    I.saveScreenshot(contador + '_PrestaLuka_' + numero + '_Home' + '_' + '.png', true);
  }
}
