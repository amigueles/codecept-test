
'use strict';

let I;
let dateformat;

module.exports = {

  _init() {
	I          = require('../steps_file.js')();
	dateformat = require('dateformat');
  },

  // insert your locators and methods here
  validateRecarga(contador, numero) {
    I.click({id: 'recargar'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.wait(3);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_Paso1' + '_' + '.png', true);

    //this.selecionarRecarga500(contador, numero);
    this.selecionarRecarga1000(contador, numero);
    this.selecionarRecarga2000(contador, numero);
    this.selecionarRecarga3000(contador, numero);
    this.selecionarRecarga5000(contador, numero);
    this.selecionarRecarga10000(contador, numero);
    this.selecionarRecarga15000(contador, numero);
    this.selecionarRecarga20000(contador, numero);

  },

  selecionarRecarga500(contador, numero) {
    I.scrollPageToBottom();
    I.wait(2);
    I.click('div[class="nice-select selectpicker"]');
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$5.00' + '_' + '.png', true);
    I.click('li[data-value="00500"]');
    I.wait(2);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_confirmar' + '_' + '.png', true);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$500_' + numero + '_Paso2' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.wait(3);
  },

  selecionarRecarga1000(contador, numero) {
    I.click({id: 'prev'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$1000' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'linkQuieroRecarga'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$1000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.saveScreenshot(contador + '_Recarga_$1000_' + numero + '_Paso2' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.scrollPageToTop();
    I.wait(3);
  },

  selecionarRecarga2000(contador, numero) {
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$2.000' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'linkQuieroRecarga'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$2000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.saveScreenshot(contador + '_Recarga_$2000_' + numero + '_Paso2' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.scrollPageToTop();
    I.wait(3);
  },

  selecionarRecarga3000(contador, numero) {
    I.click({id: 'next'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$3000' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'linkQuieroRecarga'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$3000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.saveScreenshot(contador + '_Recarga_$3000_' + numero + '_Paso2' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.scrollPageToTop();
    I.wait(3);
  },

  selecionarRecarga5000(contador, numero) {
    I.click({id: 'next'});
    I.wait(2);
    I.click({id: 'next'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$5000' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'linkQuieroRecarga'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$5000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.saveScreenshot(contador + '_Recarga_$5000_' + numero + '_Paso2' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.scrollPageToTop();
    I.wait(3);
  },

  selecionarRecarga10000(contador, numero) {
    I.click({id: 'next'});
    I.wait(2);
    I.click({id: 'next'});
    I.wait(2);
    I.click({id: 'next'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$10.000' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'linkQuieroRecarga'});
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$10.000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.saveScreenshot(contador + '_Recarga_$10.000_' + numero + '_Paso2' + '_' + '.png', true);
    I.wait(2);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.scrollPageToTop();
    I.wait(3);
  },

  selecionarRecarga15000(contador, numero) {
    I.scrollPageToBottom();
    I.wait(2);
    I.click('div[class="nice-select selectpicker"]');
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$15.000' + '_' + '.png', true);
    I.click('li[data-value="15000"]');
    I.wait(2);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.saveScreenshot(contador + '_Recarga_$15.000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$15.000_' + numero + '_Paso2' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.wait(3);
  },

  selecionarRecarga20000(contador, numero) {
    I.scrollPageToBottom();
    I.wait(2);
    I.click('div[class="nice-select selectpicker"]');
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_' + numero + '_$20.000' + '_' + '.png', true);
    I.click('li[data-value="20000"]');
    I.wait(2);
    I.click('img[src="/mientel/img/cnt-webpay.jpg"]');
    I.wait(5);
    I.saveScreenshot(contador + '_Recarga_$20.000_' + numero + '_confirmar' + '_' + '.png', true);
    I.click({id: 'confirmar_paso_uno'});
    I.waitInUrl('/step2.action', 6);
    I.see('Recargas_');
    I.wait(2);
    I.saveScreenshot(contador + '_Recarga_$20.000_' + numero + '_Paso2' + '_' + '.png', true);
    I.click({id: 'boton-atras'});
    I.waitInUrl('/recarga', 6);
    I.see('Recargas_');
    I.wait(3);
  },

  screenshot() {
  	I.saveScreenshot('Recarga_' + dateformat(new Date(), 'yyyymmdd_hhMMss') +'.png', true);
  }
}